provider "aws" {
  region = "us-east-1"
}

module "nacl" {
  source = "app.terraform.io/fer1035/network-acl/aws"
  vpc_id = "vpc-12345678"

  subnet_ids = [
    "subnet-12345678",
    "subnet-87654321"
  ]
}

output "nacl" {
  value = module.nacl.nacl
}
