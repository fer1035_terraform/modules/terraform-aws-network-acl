provider "aws" {
  region = "us-east-1"
}

module "nacl" {
  source  = "app.terraform.io/fer1035/network-acl/aws"
  version = "1.0.0"

  vpc_id = "vpc-12345678"

  subnet_ids = [
    "subnet-12345678",
    "subnet-87654321"
  ]

  nacl_rules = {
    all_ingress = {
      rule_number = 100
      egress      = false
      protocol    = "tcp"
      rule_action = "allow"
      cidr_block  = "10.192.0.0/16"
      from_port   = 443
      to_port     = 443
    }
  }
}

output "nacl" {
  value = module.nacl.nacl
}
