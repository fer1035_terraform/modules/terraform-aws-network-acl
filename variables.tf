variable "vpc_id" {
  description = "The VPC ID to associate with the Network ACL."
  type        = string
}

variable "subnet_ids" {
  description = "The list of Subnet IDs to associate with the Network ACL."
  type        = list(string)
}

variable "nacl_rules" {
  description = "The map of rules to create for the Network ACL. Rules how-to and precedence: https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html"
  type        = map

  default = {
    all_ingress = {
      rule_number = 100
      egress      = false
      protocol    = -1
      rule_action = "deny"
      cidr_block  = "0.0.0.0/0"
    }
  }
}
