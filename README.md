# terraform-aws-network-acl

A Terraform module to manage AWS VPC Network ACLs.

![Authority boundary diagram](https://gitlab.com/fer1035_terraform/modules/terraform-aws-network-acl/-/raw/main/images/authority_boundaries.png)

## Default Rules

- Deny all traffic inbound and outbound to begin with
- Allow all ICMP traffic outbound
- Allow SSH traffic from within the VPC inbound
- Allow RDP traffic from within the VPC inbound
- Allow ephemeral TCP traffic to all destinations outbound
- Allow ephemeral UDP traffic to all destinations outbound

> IPv4 and IPv6 sources and destinations must be specified in separate rules.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.5 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 5.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | ~> 5.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_network_acl.nacl](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl) | resource |
| [aws_network_acl_association.association](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_association) | resource |
| [aws_network_acl_rule.all_egress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_network_acl_rule.all_icmp_egress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_network_acl_rule.all_ingress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_network_acl_rule.ephemeral_tcp_egress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_network_acl_rule.ephemeral_udp_egress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_network_acl_rule.rule](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_network_acl_rule.vpc_rdp_ingress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_network_acl_rule.vpc_ssh_ingress](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl_rule) | resource |
| [aws_vpc.vpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_nacl_rules"></a> [nacl\_rules](#input\_nacl\_rules) | The map of rules to create for the Network ACL. Rules how-to and precedence: https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html | `map` | <pre>{<br>  "all_ingress": {<br>    "cidr_block": "0.0.0.0/0",<br>    "egress": false,<br>    "protocol": -1,<br>    "rule_action": "deny",<br>    "rule_number": 100<br>  }<br>}</pre> | no |
| <a name="input_subnet_ids"></a> [subnet\_ids](#input\_subnet\_ids) | The list of Subnet IDs to associate with the Network ACL. | `list(string)` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The VPC ID to associate with the Network ACL. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_nacl"></a> [nacl](#output\_nacl) | The details of the Network ACL. |
