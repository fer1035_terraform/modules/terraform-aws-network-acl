data "aws_vpc" "vpc" {
  id = var.vpc_id
}

resource "aws_network_acl_rule" "all_ingress" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 1
  egress         = false
  protocol       = -1
  rule_action    = "deny"
  cidr_block     = "0.0.0.0/0"
}

resource "aws_network_acl_rule" "all_egress" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 1
  egress         = true
  protocol       = -1
  rule_action    = "deny"
  cidr_block     = "0.0.0.0/0"
}

resource "aws_network_acl_rule" "all_icmp_egress" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 2
  egress         = true
  protocol       = 1
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  icmp_type      = -1
  icmp_code      = -1
}

resource "aws_network_acl_rule" "vpc_ssh_ingress" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 3
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = data.aws_vpc.vpc.cidr_block
  from_port      = 22
  to_port        = 22
}

resource "aws_network_acl_rule" "vpc_rdp_ingress" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 4
  egress         = false
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = data.aws_vpc.vpc.cidr_block
  from_port      = 3389
  to_port        = 3389
}

resource "aws_network_acl_rule" "ephemeral_tcp_egress" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 3
  egress         = true
  protocol       = "tcp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}

resource "aws_network_acl_rule" "ephemeral_udp_egress" {
  network_acl_id = aws_network_acl.nacl.id
  rule_number    = 4
  egress         = true
  protocol       = "udp"
  rule_action    = "allow"
  cidr_block     = "0.0.0.0/0"
  from_port      = 1024
  to_port        = 65535
}
