resource "aws_network_acl" "nacl" {
  vpc_id = var.vpc_id
}

resource "aws_network_acl_association" "association" {
  for_each = toset(var.subnet_ids)

  network_acl_id = aws_network_acl.nacl.id
  subnet_id      = each.value
}

resource "aws_network_acl_rule" "rule" {
  for_each = var.nacl_rules

  network_acl_id  = aws_network_acl.nacl.id
  rule_number     = each.value.rule_number
  egress          = each.value.egress
  protocol        = each.value.protocol
  rule_action     = each.value.rule_action
  cidr_block      = each.value.cidr_block
  from_port       = try(each.value.from_port, null)
  to_port         = try(each.value.to_port, null)
  ipv6_cidr_block = try(each.value.ipv6_cidr_block, null)
  icmp_type       = try(each.value.icmp_type, null)
  icmp_code       = try(each.value.icmp_code, null)
}
