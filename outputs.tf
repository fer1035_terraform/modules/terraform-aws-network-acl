output "nacl" {
  description = "The details of the Network ACL."
  value       = aws_network_acl.nacl
}
